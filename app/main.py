import os

from app.image_loader import ImageLoader
from app.image_classificator import ImageClassificator
import numpy as np


train_data_dir = os.path.join("data", "GTSRB", "Final_Training", "Images")
test_data_dir = os.path.join("data", "GTSRB", "Final_Test", "Images")
test_data_class_file = os.path.join('data', 'GT-final_test.csv')

img_size = 28

image_loader = ImageLoader(img_size, train_data_dir, test_data_dir, test_data_class_file)
train_labels, train_images = image_loader.prepare_images_for_training()

print(np.shape(train_images))
print("Train labels ", np.shape(train_labels))

num_classes = 43
img_size_flat = image_loader.img_size_flat


image_classificator = ImageClassificator(img_size, img_size_flat, num_classes, train_images, train_labels)

image_classificator.build_graph()
image_classificator.create_tf_session()
image_classificator.optimize(1000)

test_labels, test_images = image_loader.prepare_images_for_test()
image_classificator.test_accuracy(test_labels, test_images)
image_loader.prepare_images_for_training()

